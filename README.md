# usernamegen

This is a quick script to help generate a memorable username.

## Install
`pip install git+https://gitlab.com/cmhedrick/usernamegen.git`

## Use
```bash
$ generate-username
intimidatedmap
```