from usernamegen import UsernameGen


class TestUsernameGen:
    username_gen = UsernameGen()

    def test_get_adjective(self):
        assert self.username_gen.adjective

    def test_get_noun(self):
        assert self.username_gen.noun

    def test_get_username(self):
        assert self.username_gen.username
