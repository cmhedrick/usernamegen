#!/usr/bin/env python

import os
import random


class UsernameGen:
    def __init__(self):
        self.path = os.path.dirname(__file__)
        self.adjective = self.get_adjective()
        self.noun = self.get_noun()
        self.username = self.adjective + self.noun

    def get_adjective(self):
        # get adjective word
        # return: str
        with open(os.path.join(self.path, "adjectives.txt")) as f:
            lines = f.readlines()
            return random.choice(lines).strip()

    def get_noun(self):
        # get noun word
        # return: str
        with open(os.path.join(self.path, "nouns.txt")) as f:
            lines = f.readlines()
            return random.choice(lines).strip()

    def __str__(self):
        return self.username

    def __repr__(self):
        return self.username


if __name__ == "__main__":
    print(UsernameGen())
